package com.epam.lab;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "ACa0c23ee056caa2eb765e9c439cd06dea";
    public static final String AUTH_TOKEN = "6cbcca93f084f9b30ac53d1ea8b3fc43";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380671789063"), /*my phone number*/
                        new PhoneNumber("+16787234533"), str).create(); /*attached to me number*/
    }
}

